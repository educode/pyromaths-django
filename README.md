[Pyromaths](http://pyromaths.org/) est un programme qui a pour but de créer des exercices type de mathématiques niveau collège et lycée ainsi que leur corrigé.

Ce projet est la version [en ligne](http://enligne.pyromaths.org) de Pyromaths.

# Installation

1. Créer un *virtualenv*.
2. Installer les dépendances.

        pip3 install -r requirements.txt

# Initialisation

## Base de données

Puisque ce site web n'utilise pas de base de données, il n'y a aucune initialisation à faire de ce côté là.

## Fichier ``enligne/settings.py``

1. Copier le fichier ``enligne/settings.py.example`` en ``enligne/settings.py`` (pour la version de développement, en faire un lien symbolique suffit).
2. Pour la version en production, dans ce nouveau fichier :

  1. changer la ligne ``DEBUG = True`` en ``DEBUG = FALSE`` ;
  2. définir une clef secrète ``SECRET_KEY``. Celle-ci peut être générée avec le script suivant.

    ```Python
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    print('SECRET_KEY = "' + get_random_string(50, chars) + '"')
    ```

# Lancement

    python3 manage.py runserver
