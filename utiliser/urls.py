from django.conf.urls import url

from . import views

app_name = "utiliser"

urlpatterns = [
    url(r'^$', views.choix, name="choix"),
    url(r'^creer/', views.creer, name="creer"),
    ]
