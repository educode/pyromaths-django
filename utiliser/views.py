import functools
import itertools
import os

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.urls import reverse

from pyromaths.ex import ExerciseBag
from pyromaths.outils.System import Fiche
from pyromaths.version import VERSION

from datetime import date

from . import MAINSITEURL

BAG = ExerciseBag()
EXERCICES = [
        (
            niveau,
            [(exo.name(), exo.description()) for exo in exercices]
        )
        for niveau, exercices in BAG.sorted_levels()
        ]

def choix(request):
    return render(request, 'utiliser/utiliser.html', {
        'VERSION': VERSION,
        'DOWNLOADURL': VERSION.replace('.', '-'),
        'EXERCICES': EXERCICES,
        'MAINSITEURL': MAINSITEURL,
        'deficit': 1888.98+11.99*(date.today().year*12 + date.today().month-12*2014-6)
        }
        )

def _first(length=1):
    def decorator_first(func):
        @functools.wraps(func)
        def wrapper_first(*args, **kwargs):
            return itertools.islice(func(*args, **kwargs), length)
        return wrapper_first
    return decorator_first

@_first(5)
def parseSeeds(*arguments):
    """Itérateur sur (au maximum) cinq 'seeds' d'exercice."""
    for argument in arguments:
        for text in argument.split(","):
            try:
                yield int(text)
            except ValueError:
                pass

def creer(request):
    # Génération de la liste d'exercices
    exercices = []
    for key in request.GET:
        if key in BAG:
            for seed in parseSeeds(*request.GET.getlist(key)):
                exercices.append(BAG[key](seed))
    # Génération du PDF
    if exercices:
        parametres = {
            'enonce': True,
            'corrige': True,
            'exercices': exercices,
            'titre': "Fiche de révisions",
            }
        with Fiche(parametres) as fiche:
            fiche.write_pdf()

            # Envoi du PDF
            with open(fiche.pdfname, mode="rb") as pdf:
                response = HttpResponse(content_type = 'application/pdf')
                response['Content-Disposition'] = 'attachment; filename=exercices.pdf'
                response['Content-Length'] = os.path.getsize(fiche.pdfname)
                response.write(pdf.read())
                return response
    else:
        messages.error(request, "Vous devez sélectionner au moins un exercice.")
        return redirect("utiliser:choix")
